var bunny;
var actionList = new Array();
var actionId = 0;
var collected;

var playfield;

function run() {
	$("#playfield").keydown(keyHandler);

	// TODO new interface, playfield is outdated, on start request with genesis action
	// and on reply do the genesisize

	var url = "/Server/Playfield.php";
	$.ajax({ type:"GET", url:url, data:'', complete:runCallback });
	return false;
}

var runCallback = function(res, status) {
	if (status != "success") {
  		alert("runCallback failed");
  		return;
  	}

	var data = xmlToDOM(res.responseText);

	var actions = data.getElementsByTagName('Action');
	
	for (var i = 0; i < actions.length; i++) {
        var type = actions[i].getElementsByTagName('Type')[0];
        if (type.textContent == "genesis") {
     		genesisize(actions[i]);   	
        }
    }
}

// sends data to server
function send(data) {
	data = "xml=" + data;
	var url = "/Server/PlayerHandler.php";
	$.ajax({ type:"POST", url:url, data:data, complete:sendCallback });
	return false;
}

var sendCallback = function(res, status) {
	if (status != "success") {
  		alert("sendCallback failed");
  		return;
  	}

	var data = xmlToDOM(res.responseText);
	
	var messages = data.getElementsByTagName('Message');
	for (var i = 0; i < messages.length; i++) {
        var message = messages[i].textContent;
        alert(message);
	}
	
	var actions = data.getElementsByTagName('Action');
	
	for (var i = 0; i < actions.length; i++) {
        
        var id = actions[i].getAttribute('id');
        var confirmed = actions[i].getElementsByTagName('Confirmed')[0].textContent == 'true';

		if (id == actionList[0].id) {
			if (confirmed) {
				// remove confirmed action 
				actionList.shift();
			} else {
				// undo all the actions so far
				for (var j = actionList.length - 1; j >= 0; j--) {
					var success = doAction(actionList[j], true);
					if (!success) {
						alert("action could not be undone!");
						return;
					}
				}
				actionList.length = 0;
				return;
			}
		} else {
			alert("confirmation for nonexisting action!");
			return;
		}
    }
}

// handles genesis action
function genesisize(action) {

	var playfieldXML = action.getElementsByTagName('Playfield')[0];
	
	playfield = generatePlayfield(action);
	generatePlayfieldDOM(playfield);
    
	var player = action.getElementsByTagName('Player')[0];
	var playerX = player.getElementsByTagName('PositionX')[0].textContent;
	var playerY = player.getElementsByTagName('PositionY')[0].textContent;
	collected = player.getElementsByTagName('Carrots')[0].textContent;
	 
	bunny = { x: parseInt(playerX), y: parseInt(playerY) };
	$("#" + bunny.x + "x" + bunny.y).attr('class', 'player');
	updateCollected(collected);
}

// converts the XML data to JS playfield object
function generatePlayfield(dataXML) {

	var playfieldXML = dataXML.getElementsByTagName('Playfield')[0];
	
	var sizeX = playfieldXML.getElementsByTagName('SizeX')[0].textContent;
	var sizeY = playfieldXML.getElementsByTagName('SizeY')[0].textContent;

	var playfield = new Array(sizeY);
	for (var j = 0; j < sizeY; j++) {
		playfield[j] = new Array(sizeX);
		for (var i = 0; i < sizeX; i++) {
			playfield[j][i] = { type:'empty', object:''};
		}
	}

	var fields = dataXML.getElementsByTagName('Field');

	for (var i = 0; i < fields.length; i++) {

		var x = parseInt(fields[i].getAttribute('x'));
		var y = parseInt(fields[i].getAttribute('y'));
		var type = fields[i].getElementsByTagName('Type')[0].textContent;
		
		playfield[y][x].type = type;

		var objects = fields[i].getElementsByTagName('Object');
		if (objects.length > 0) {
			playfield[y][x].object = objects[0].textContent;
		} 
    }
    
    return playfield;
}

// Clears the current field and generates a new, empty one sized sizeX * sizeY.
function generatePlayfieldDOM(playfield) {

	var html = '';
	
	for (var j = 0; j < playfield.length; j++) {

		html += '<tr>';
		for (var i = 0; i < playfield[j].length; i++) {
			html += '<td id="' + i + 'x' + j + '" class = "empty"/>';
		}
		html += '</tr>';
	}
	
	$("#playfield").html(html); 

	for (var j = 0; j < playfield.length; j++) {
		for (var i = 0; i < playfield[j].length; i++) {
			generateFieldDOM(playfield[j][i], i, j);
		}
	}
}

// updates one DOM field
function generateFieldDOM(field, x, y) {

	var cssClass = field.type;
	if (field.object == 'carrot' && cssClass != 'player') {
		cssClass = field.object;
	}
	$("#" + x + "x" + y).attr('class', cssClass);
}

function updateCollected(collected) {
	$("#collected").html(collected);
}

function keyHandler(event) {

	var actionMap = {
		'37': 'left',
		'38': 'up',
		'39': 'right',
		'40': 'down',
		'13': 'collect'
	}

	var code = event.keyCode;
	if (code in actionMap) {
		var action = {
			id: actionId,
		    type: actionMap[code]
		}
	
		var success = doAction(action);
		if (!success) return;
		 
		sendAction(action);
	}
}

function sendAction(action) {

	var xmlAction = '<?xml version="1.0" encoding="UTF-8"?>'
	    + '<GenGame>'
		+ '<Action id="' + actionId + '" source="1">'
		+ '<Type>' + action.type + '</Type>'
		+ '</Action>'
	    + '</GenGame>';

	actionList.push(action);
	actionId++;
	
	send(xmlAction);
}

/* Executes the action. If optional parameter undo is true, the action is
 * undone instead.
 * Returns true if the action was done, and false if it could not be done.
 */ 
function doAction(action, undo) {

	var type = action.type;
	if (type == 'collect') {
	    return doCollectAction(action, undo);
	} else {
	    return doMoveAction(action, undo);
	}
}

// executes the collect action
function doCollectAction(action, undo) {

	undo = (typeof undo == "undefined") ? false : undo

	var bunnyField = playfield[bunny.y][bunny.x];

	if (!undo) {
		if (bunnyField.object != 'carrot') return false;
		collected++;
		bunnyField.object = '';
	} else {
		if (bunnyField.object != '') return false;
		collected--;
		bunnyField.object = 'carrot';
	}
	
	generateFieldDOM(bunnyField, bunny.x, bunny.y);
	updateCollected(collected);

	return true;
}

// executes the move action 
function doMoveAction(action, undo) {

	undo = (typeof undo == "undefined") ? false : undo

	var type = action.type;
	if (undo) {
		var undoMap = {
			'left':  'right',
			'up':    'down',
			'right': 'left',
			'down':  'up'
		}
	
		type = undoMap[type];
	}

	var directions = {
		'left':  { x: -1, y:  0},
		'up':    { x:  0, y: -1},
		'right': { x:  1, y:  0},
		'down':  { x:  0, y:  1}
	}
	
	var direction = directions[type];
	
	if (!direction) {
		alert("unknown action!");
	}
	
	var newBunny = {
		x: bunny.x + direction.x,
		y: bunny.y + direction.y
	}

	var newBunnyField = playfield[newBunny.y][newBunny.x];

	if (newBunnyField.type != 'empty' && newBunnyField.object != 'carrot') {
		return false;
	}

	playfield[bunny.y][bunny.x].type = 'empty';
	generateFieldDOM(playfield[bunny.y][bunny.x], bunny.x, bunny.y);

	playfield[newBunny.y][newBunny.x].type = 'player';
	generateFieldDOM(playfield[newBunny.y][newBunny.x], newBunny.x, newBunny.y);

	bunny = newBunny;
	
	return true;	
}

function xmlToDOM(xml) {
    if (window.DOMParser) {
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(xml, "text/xml");
  	} else {
  	    // Internet Explorer 
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = false;
        xmlDoc.loadXML(xml);
   }
   return xmlDoc;
}
