<?xml version="1.0" encoding="UTF-8"?>
<GenGame>
<?php
include "xml2array.php";

$xml = '<?xml version="1.0" encoding="UTF-8"?>
        <GenGame>
         <Action id="1" source="1">
          <Type>bunny right</Type> 
         </Action>
         <Action id="2" source="1">
          <Type>bunny up</Type> 
         </Action>
        </GenGame>';

 //PARSE XML INTO ARRAY
 $parser = new xml2Array();
 $parsed = $parser->parse($xml);

 //EACH GenGame/Action
 foreach ($parsed[0]["children"] as $item ) {
  if($item["name"] == "ACTION") {

?>
 <Action id="<?php echo $item["attrs"]["ID"]; ?>" source="<?php echo $item["attrs"]["SOURCE"]; ?>">
<?php

   //EACH GenGame/Action[x]/Type (THERE SHOULD BE JUST ONE)
   foreach ($item["children"] as $actionData) {
    if($actionData["name"] == "TYPE") {
     if($actionData["tagData"] == "bunny up") {
?>
  <Confirmed>false</Confirmed>
<?php
     } else {
?>
  <Confirmed>true</Confirmed>
<?php
     }
    }
?>
 </Action>
<?php
   }
  }
 } 
?>
</GenGame>
