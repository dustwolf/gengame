<?xml version="1.0" encoding="UTF-8"?>
<GenGame>
 <Action id="1" source="0">
  <Type>genesis</Type>
  <Change>
<?php 
   /* ALL THIS STUFF SHOULD GO IN RESPONSE TO GENESIS REQUEST IN PlayerHandler.php */

   //CONNECT DB
   include "Settings.php";
   $sql = mysql_connect($DBHost,$DBUser,$DBPassword) or die(mysql_error());
   mysql_select_db($DBSchema, $sql);

   //GET PLAYER COORDINATES FROM DB
   $PlayerX = mysql_result(mysql_query(
    "SELECT Value FROM Settings WHERE Property = 'PlayerX'"
   ),0);

   $PlayerY = mysql_result(mysql_query(
    "SELECT Value FROM Settings WHERE Property = 'PlayerY'"
   ),0);

   $InfotronsPicked = mysql_result(mysql_query(
    "SELECT Value FROM Settings WHERE Property = 'InfotronsPicked'"
   ),0);

   //SEND PLAYER COORDINATES TO CLIENT
?>
   <Playfield>
    <SizeX><?php echo $PlayfieldSizeX; ?></SizeX>
    <SizeY><?php echo $PlayfieldSizeY; ?></SizeY>
   </Playfield>

   <Player>
    <PositionX><?php echo $PlayerX; ?></PositionX>
    <PositionY><?php echo $PlayerY; ?></PositionY>
    <Carrots><?php echo $InfotronsPicked; ?></Carrots> 
   </Player>

<?php 

    //DUMP PLAYFIELD
    for($x = 0; $x < $PlayfieldSizeX; $x++) {
     for($y = 0; $y < $PlayfieldSizeY; $y++) {
      $coords = $x."x".$y;
      
      $t = mysql_result(mysql_query(
       "SELECT state FROM Playfield WHERE coordinates = '".mysql_real_escape_string($coords)."'"
      ),0);
      $o = mysql_result(mysql_query(
       "SELECT object FROM Playfield WHERE coordinates = '".mysql_real_escape_string($coords)."'"
      ),0);
      echo '   <Field x="'.$x.'" y="'.$y.'">'."\n";
      echo '    <Type>'.$PlayfieldCodes[$t].'</Type>'."\n";
      if($o != '') echo '    <Object>'.$PlayfieldCodes[$o].'</Object>'."\n";
      echo '   </Field>'."\n";
     }
    }

   mysql_close($sql);
?>

  </Change>
 </Action>
</GenGame>
