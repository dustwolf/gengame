<?xml version="1.0" encoding="UTF-8"?>
<GenGame>
<?php
 //INIT DB
 include "Settings.php";
 $sql = mysql_connect($DBHost,$DBUser,$DBPassword) or die(mysql_error());
 mysql_select_db($DBSchema, $sql);

 //INIT XML
 include "xml2array.php";

 $xml = str_replace("\\","",$_POST["xml"]);

 //PARSE XML INTO ARRAY
 $parser = new xml2Array();
 $parsed = $parser->parse($xml);

 //EACH GenGame/Action
 foreach ($parsed[0]["children"] as $item ) {
  if($item["name"] == "ACTION") {

?>
 <Action id="<?php echo $item["attrs"]["ID"]; ?>" source="<?php echo $item["attrs"]["SOURCE"]; ?>">
<?php

   //EACH GenGame/Action[x]/Type (THERE SHOULD BE JUST ONE)
   foreach ($item["children"] as $actionData) {
    if($actionData["name"] == "TYPE") {
    
      ///THIS SHOULD IN FACT PASS COMMANDS TO INPUT LOG(i) AND OUTPUT FROM OUTPUT LOG(o) 

 
      //GET PLAYER COORDINATES FROM DB
      $PlayerX = mysql_result(mysql_query(
       "SELECT Value FROM Settings WHERE Property = 'PlayerX'"
      ),0);

      $PlayerY = mysql_result(mysql_query(
       "SELECT Value FROM Settings WHERE Property = 'PlayerY'"
      ),0);

     //CALCULATE HYPOTHETICAL PLAYER MOVE
     $Collect = false;
     $NewX = $PlayerX;
     $NewY = $PlayerY;     

     switch ($actionData["tagData"]) {
      case "right":
       $NewX++;
       break;
      case "left":
       $NewX--;
       break;
      case "down":
       $NewY++;
       break;
      case "up":
       $NewY--;
       break;
      case "collect":
       $Collect = true;
       break;
     }

     //VALIDATE
     $MoveValid = false;

     if($Collect) {
      //DETERMINE FIELDS
      $PlayerField = mysql_result(mysql_query(
       "SELECT state FROM Playfield WHERE coordinates = '".mysql_real_escape_string($PlayerX."x".$PlayerY)."'"
      ),0);

      $InfotronField = mysql_result(mysql_query(
       "SELECT object FROM Playfield WHERE coordinates = '".mysql_real_escape_string($PlayerX."x".$PlayerY)."'"
      ),0);

      //VALIDATE THAT PLAYER AND CARROT EXISTS HERE
      if($PlayerField == "player" or $PlayerField == "") {
       if($InfotronField == "+") $MoveValid = true;
      }

     } else {
      //DETERMINE FIELDS
      $PlayerField = mysql_result(mysql_query(
       "SELECT state FROM Playfield WHERE coordinates = '".mysql_real_escape_string($PlayerX."x".$PlayerY)."'"
      ),0);
 
      $NewField = mysql_result(mysql_query(
       "SELECT state FROM Playfield WHERE coordinates = '".mysql_real_escape_string($NewX."x".$NewY)."'"
      ),0);

      //VALIDATE THAT PLAYER EXISTS HERE AND HAS MOVED TO AN EMPTY SPOT
      if($PlayerField == "player" or $PlayField == "") {
       if($NewField == "") {
        if($NewX >= 0 and $NewX < $PlayfieldSizeX and $NewY >= 0 and $NewY < $PlayfieldSizeY) $MoveValid = true;
       }
      }
     }

     if($MoveValid) {
     //UPDATE FIELDS IN DATABASE
      if($Collect) {
       //CLEAR INFOTRON
       mysql_query(
        "UPDATE Playfield SET object='' WHERE coordinates='".mysql_real_escape_string($PlayerX."x".$PlayerY)."';"
       ) or $debug = mysql_error();

       //++ NUMBER OF INFOTRONS PICKED
       mysql_query(
        "UPDATE Settings SET Value = (1 + Value) WHERE Property='InfotronsPicked';"
       );

      } else {
       //MOVE PLAYER
       mysql_query(
        "UPDATE Playfield SET state='player' WHERE coordinates='".mysql_real_escape_string($NewX."x".$NewY)."';"
       ) or $debug = mysql_error();
       mysql_query(
        "UPDATE Playfield SET state='' WHERE coordinates='".mysql_real_escape_string($PlayerX."x".$PlayerY)."';"
       ) or $debug = mysql_error();
      }

      //UPDATE PLAYER COORDINATES IN DATABASE
      $PlayerX = $NewX;
      $PlayerY = $NewY;
  
      mysql_query(
       "UPDATE Settings SET Value='".$PlayerX."' WHERE Property='PlayerX';"
      );
 
      mysql_query(
       "UPDATE Settings SET Value='".$PlayerY."' WHERE Property='PlayerY';"
      );

      //APPROVE
?>
  <Confirmed>true</Confirmed>
<?php
     } else {
?>
  <Confirmed>false</Confirmed>
<?php
     }
    }
   if(strlen($debug) > 0) {
    echo "<Message>".$debug."</Message>";
   }
?>
 </Action>
<?php
   }
  }
 } 
//CLOSE DB
mysql_close($sql);
?>
</GenGame>
