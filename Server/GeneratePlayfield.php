<?php
include "Settings.php";
$sql = mysql_connect($DBHost,$DBUser,$DBPassword) or die();
mysql_select_db($DBSchema, $sql);

//LOCATE PLAYER
$PlayerX = rand(0,$PlayfieldSizeX-1);
$PlayerY = rand(0,$PlayfieldSizeY-1);

mysql_query(
 "UPDATE Settings SET Value='".$PlayerX."' WHERE Property='PlayerX';"
);

mysql_query(
 "UPDATE Settings SET Value='".$PlayerY."' WHERE Property='PlayerY';"
);

//SET DYNAMIC WORLD PROPERTIES
$infotrons = round(($PlayfieldSizeX + $PlayfieldSizeY)/2);
$NumberOfWalls = $infotrons;

mysql_query(
 "UPDATE Settings SET Value='".$infotrons."' WHERE Property='Infotrons';"
);

mysql_query(
 "UPDATE Settings SET Value='".$NumberOfWalls."' WHERE Property='NumberOfWalls';"
);

//RESET PLAYER STATE
mysql_query(
 "UPDATE Settings SET Value='0' WHERE Property='InfotronsPicked';"
);

//ERASE AND RECREATE
mysql_query(
 "DROP TABLE `Playfield`;"
);

mysql_query(
 "CREATE TABLE `Playfield` (
   `coordinates` VARCHAR(20)  NOT NULL,
   `state` VARCHAR(20) ,
   `object` VARCHAR(200) ,
   PRIMARY KEY (`coordinates`)
 )
 ENGINE = MyISAM
 CHARACTER SET utf8 COLLATE utf8_general_ci;"
);

//LEVEL BUFFER INIT
for($x = 0; $x <= $PlayfieldSizeX; $x++) {
 for($y = 0; $y <= $PlayfieldSizeY; $y++) {
  $level[$x][$y] = "";
 }
}

//MAZEGEN
for($walls = 1; $walls <= $NumberOfWalls; $walls++) {
 $wallX = rand(0,$PlayfieldSizeX-1);;
 $wallY = rand(0,$PlayfieldSizeY-1);;
 for($iteration = 1; $iteration <= 100; $iteration++) {
  $level[$wallX][$wallY] = "wall";
  $testcounter = 0;
  $sX = $wallX; $sY = $wallY;
  switch(round(rand(0,3))) {
   case 0:
    $sX--; 
    break;
   case 1: 
    $sX++;
    break;
   case 2: 
    $sY--;
    break;
   case 3: 
    $sY++;
    break;
  }
  if($level[$sX-1][$sY] == "wall") $testcounter++;
  if($level[$sX+1][$sY] == "wall") $testcounter++;
  if($level[$sX][$sY-1] == "wall") $testcounter++;
  if($level[$sX][$sY+1] == "wall") $testcounter++;
  if(rand(0,1)==0) {
   if($level[$sX+1][$sY+1] == "wall") $testcounter++;
   if($level[$sX+1][$sY-1] == "wall") $testcounter++;
   if($level[$sX-1][$sY+1] == "wall") $testcounter++;
   if($level[$sX-1][$sY-1] == "wall") $testcounter++;
  }
 
  if($testcounter == 1 && $sX >= 0 && $sY >= 0 && $sX < $PlayfieldSizeX && $sY < $PlayfieldSizeY) {
   $wallX = $sX;
   $wallY = $sY;
  }
 }
}

//PLACE INFOTRONS IN A WAY THAT INCREASES SOLVABILITY
$infotron = 0; $insanity = 0;
while($infotron <= $infotrons && $insanity < 1000) {
 $insanity++;
 for($x = 1; $x <= $PlayfieldSizeX-2; $x++) {
  for($y = 1; $y <= $PlayfieldSizeY-2; $y++) {
   if($level[$x][$y] == "wall" && rand(0,5)==0 && $infotron < $infotrons) {
    $testcounter=0;
    if($level[$x-1][$y] == "") $testcounter++;
    if($level[$x+1][$y] == "") $testcounter++;
    if($level[$x][$y-1] == "") $testcounter++;
    if($level[$x][$y+1] == "") $testcounter++;
    if($testcounter == 2) {
     $infotron++;
     $level[$x][$y] = "+";
    }
   }
  }
 }
}

//RENDER INTO DB
for($x = 0; $x < $PlayfieldSizeX; $x++) {
 for($y = 0; $y < $PlayfieldSizeY; $y++) {
  $s = $level[$x][$y];
  //if($x == $PlayerX and $y == $PlayerY) $s = "player"; //PLACE PLAYER ON PLAYFIELD

  $coords = $x."x".$y;

  if($s == "+") {
   mysql_query(
    "INSERT INTO `Playfield` 
      (`coordinates`,`state`,`object`)
      VALUES ('".mysql_real_escape_string($coords)."', '', '+');"
   );
  } else {
   mysql_query(
    "INSERT INTO `Playfield` 
      (`coordinates`,`state`,`object`)
      VALUES ('".mysql_real_escape_string($coords)."', '".mysql_real_escape_string($s)."', '');"
   );
  }
 }
}

mysql_close($sql);
header('Location: /');
?>
